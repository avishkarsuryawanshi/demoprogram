package com.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class RemoveAllDemo {

	public static void main(String[] args) {
		Collection<Integer> firstCollection = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
			    Collection<Integer> secondCollection = new ArrayList<>(
			      Arrays.asList(3, 4, 5, 6, 7));

			   firstCollection.removeAll(secondCollection);
			    
			    firstCollection.clear();
			    System.out.println(firstCollection);

			    
	}

}
